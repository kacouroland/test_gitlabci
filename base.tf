variable "region" {
      description = "The AWS Region"
      default = "us-west-2"
}

variable "ami" {
     type = "map"
     default = {
        us-west-2 = "ami-6df1e514"
        }
description = "The AMI to use."
} 
